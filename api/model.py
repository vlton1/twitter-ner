import json
import pandas as pd
from transformers import AutoTokenizer, AutoModelForTokenClassification
from transformers import pipeline


def preload_model():

    print('Loading pretrain model...')
    tokenizer = AutoTokenizer.from_pretrained(
        "./api/twitter-roberta-base-dec2021-tweetner7-random", local_files_only=True)
    model = AutoModelForTokenClassification.from_pretrained(
        "./api/twitter-roberta-base-dec2021-tweetner7-random", local_files_only=True)
    return model, tokenizer


def inference(text, model, tokenizer):

    nlp = pipeline('ner', model=model, tokenizer=tokenizer,
                   aggregation_strategy="simple")
    result_json = nlp(text)
    for item in result_json:
        item['score'] = round(float(item['score']), 2)
    return result_json
