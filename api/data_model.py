"""
Data Modeling of the library
"""

from pydantic import BaseModel
from typing import List, Optional, Union


class InputDataModel(BaseModel):
    text: str


class OutputModel(BaseModel):
    text: str
    output: list


class ResponseModel(BaseModel):
    status: int
    message: str
    data: Optional[OutputModel]
