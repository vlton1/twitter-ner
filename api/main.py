"""
Main API Serving Library
"""

import logging
import gc
from fastapi import FastAPI, Depends, Body
from fastapi.security.api_key import APIKey
from api.data_model import InputDataModel, ResponseModel
from api.model import inference, preload_model
from api.core.utils.check_auth import check_authentication_header
from starlette.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR

app = FastAPI(
    title='Twitter Keyword Extraction API',
    description='Keyword extraction for twitter posts',
)

logger = logging.getLogger("app")


try:
    model, tokenizer = preload_model()
except:
    raise Exception("model can not be loaded")


@app.get('/')
def helloword():
    response = ResponseModel(status=HTTP_200_OK,
                             message='hello world')
    return response


@app.post('/extract')
async def keyword_model(data: InputDataModel = Body(..., embed=False),
                        api_key: APIKey = Depends(check_authentication_header)):

    try:
        if data.text == "":
            response = ResponseModel(status=HTTP_400_BAD_REQUEST,
                                     message=str("Text can not be null"))
            return response
        else:
            result_json = inference(data.text, model, tokenizer)
            print(type(result_json))
            output = {"text": data.text, "output": result_json}
            response = ResponseModel(status=HTTP_200_OK,
                                     message="successful",
                                     data=output)
            return response.dict()

    except Exception as e:
        response = ResponseModel(status=HTTP_500_INTERNAL_SERVER_ERROR,
                                 message=str(e))
        print(e)
        return response
