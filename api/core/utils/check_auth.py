import logging
import os
from fastapi import Depends, HTTPException
from starlette.status import HTTP_401_UNAUTHORIZED
from fastapi.security.api_key import APIKeyHeader

logger = logging.getLogger("app")

# ===== API Key =====
API_KEY = "api_token"
get_api_key = APIKeyHeader(name="x-api-key")


def check_authentication_header(api_key: str = Depends(get_api_key)):
    """ takes the X-API-Key header and converts it into the matching user object from the database """
    logger.info("x-api-key: %s", api_key)
    if api_key in API_KEY:
        return api_key
    raise HTTPException(
        status_code=HTTP_401_UNAUTHORIZED,
        detail="Invalid API Key",
    )
